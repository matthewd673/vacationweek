import os

#enter a file to run
file = input("Enter a file to run: ")

#some files cannot be run, so we catch any errors
try:
	os.startfile(file)
except:
	print("An error occurred when running the file")