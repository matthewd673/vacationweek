#quick way to turn the lists back into a string
def tostr(chars):
	out = ""
	for i in range(len(chars)):
		out += chars[i]
	return out

#take user input
word = input("Enter a word: ")

original = list(word)

backwards = list(word)
backwards.reverse()

#output
print("Original: " + tostr(original))
print("Reversed: " + tostr(backwards))

if tostr(original) == tostr(backwards):
	print("It's a palindrome!")
else:
	print("Not a palindrome :(")