#let the user select a mode
mode = input("Select a mode (rectangle, circle, triangle): ")

#run different code depending on the mode
if mode == "rectangle":
	#get dimensions
	w = int(input("Width: "))
	h = int(input("Height: "))
	#calculate and output
	a = w * h
	print("The area is: " + str(a))
elif mode == "circle":
	#get dimensions
	pi = 3.14 #simple, but it works
	r = int(input("Radius: "))
	a = pi * (r * r)
	print("The area is: " + str(a))
elif mode == "triangle":
	#get dimensions
	w = int(input("Width: "))
	h = int(input("Height: "))
	a = (w * h) / 2
	print("The area is: " + str(a))
else:
	print("Invalid mode")