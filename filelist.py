from os import walk

#get user input
path = input("Enter a folder path: ")

#prep empty list
f = []

#use "walk" to crawl the directory and add files to a list
for (path, dirnames, filenames) in walk(path):
    f.extend(filenames)
    break

#print out list of files
for i in range(len(f)):
	print(f[i])