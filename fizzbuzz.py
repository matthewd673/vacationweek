for i in range(0, 25):
	#build onto output to save an if statement
	out = ""
	#check if no remainder (divisible)
	if i % 3 == 0:
		out += "Fizz"
	if i % 5 == 0:
		out += "Buzz"
	#print output
	print(str(i) + ": " + out)