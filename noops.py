import requests #requires "python -m pip install requests"
import json

#make the http request
r = requests.get("http://api.noopschallenge.com/hexbot")

j = json.loads(r.text) #load the response into a json object

color = j["colors"][0]["value"] #a weird way to grab the data from the json, it makes sense if you look at the structure