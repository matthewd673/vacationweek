from array import *

print("Welcome to Tic-Tac-Toe")

#setup board
board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

#enter input loop
turnCt = 0 #track turn count
while True:
	turnCt += 1

	mark = "-"
	if turnCt % 2 == 0: #even number
		mark = "O"
	else:
		mark = "X"

	#accept user input
	command = input("Enter a coordinate to place an " + mark + " (row, col): ")

	#separate out coordinates
	x = int(command.split(",")[0]) - 1
	y = int(command.split(",")[1]) - 1

	#check if the coords are valid
	if x <= 2 and x >= 0 and y <= 2 and y >= 0:
		#turn the marks into numeric values to store
		if mark == "O" and board[x][y] == 0:
			board[x][y] = 1
		if mark == "X" and board[x][y] == 0:
			board[x][y] = 2
	else:
		print("Invalid coordinates")

	#print board
	for i in range(3):
		out = ""
		for j in range(3):
			#turn the number ids back into marks
			if board[i][j] == 1:
				out += "X"
			elif board[i][j] == 2:
				out += "O"
			else:
				out += "-"
		print(out)
