import math

#get a & b values
a = int(input("A: "))
b = int(input("B: "))

#calculate c value
c = math.sqrt((a * a) + (b * b))

print("C: " + str(c))