import random

#load file
f = open("words.txt", "r")
words = f.readlines()

#create output string to build on to
out = ""

for i in range(10):
	#get next random word from list
	w = words[random.randint(0, len(words))]
	#remove newline (\n)
	w = w.replace("\n", "")
	out += w + " " #add a random word

print(out)