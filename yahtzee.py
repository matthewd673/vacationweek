import random

#a list to store counts of dice values
values = [0, 0, 0, 0, 0, 0]

#roll 5 dice
for i in range(0, 5):
	values[random.randint(0, 5)] += 1

print(values)

#check scoring
score = 0

if 3 in values:
	print("3 of a kind!")
	v = values.index(3) #determine which value we have 3 of
	score += ((v + 1) * 3) #ally up the score
elif 4 in values:
	print("4 of a kind!")
	v = values.index(4)
	score += ((v + 1) * 3) 
elif 3 in values and 2 in values:
	print("Full house!")
	va = values.index(3)
	vb = values.index(2)
	score += ((va + 1) * 3) + ((vb + 1) * 2)
elif 5 in values:
	print("Yahtzee!")
	score += 50
else: #got to be chance
	#loop through and calculate all
	for i in range(len(values)):
		score += ((i + 1) * values[i])

#output score
print("Your score was: " + str(score))