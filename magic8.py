import random

#create list of responses
responses = ["Yes", "No", "Maybe", "Probably", "Likely", "Unlikely", "Sure thing", "Can't tell", "Don't know"]

#print a random response
output = responses[random.randint(0, len(responses))] #sorry for putting it all on one line
print("The 8 ball says: " + output)